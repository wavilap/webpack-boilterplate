const path = require('path') // esto es nativo de nodejs
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackPugPlugin = require('html-webpack-pug-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const globImporter = require( 'node-sass-glob-importer' )

module.exports = {
	entry: {
		home: './src/js/index.js',
		contact: './src/js/contact.js'
	},
	output: {
		filename: 'js/[name].min.js',
		path: path.resolve(__dirname, 'dist')
	},
	devServer: {
		port: 3000
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			},
			{
        test: /\.(sass|scss|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              importer: globImporter(),
              includePaths: [
                path.resolve(__dirname, 'node_modules')
              ]
            }
          },
        ]
      },
			{
				test: /\.pug$/,
				use: {
					loader: 'pug-loader',
					options: {
						pretty: true
					}
				}
			}
		]
	},
	optimization: {
		splitChunks: {
			cacheGroups: {
				commons: {
					test: /[\\/]node_modules[\\/].*\.js$/,
					name: 'vendor',
					chunks: 'all',
					minChunks: 2
				}
			}
		}
	},
	plugins: [
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'src/pug/views/index.pug',
			chunks: ['home', 'vendor']
		}),
		new HtmlWebpackPlugin({
			filename: 'contacto.html',
			template: 'src/pug/views/contacto.pug',
			chunks: ['contact', 'vendor']
		}),
		new HtmlWebpackPugPlugin({
			adjustIndent: true
		}),
		new MiniCssExtractPlugin({
			filename: 'css/main.min.css'
		}),
		// new webpack.LoaderOptionsPlugin({ options: { debug: true } }),
		new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.$': 'jquery',
      'window.jQuery': 'jquery'
    })
	]
}

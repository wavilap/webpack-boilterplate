import 'jquery-validation'
import '../libs/filter-input'
import videojs from 'video.js'
import 'videojs-youtube/dist/Youtube'
import 'magnific-popup'
import 'slick-carousel';

const Template = {}

const ClassName = {
	ANIMATE_HEADER_VIDEO			: 'animate-headerVideo',
	ANIMATE_DELAY							: 'animate-delay',
	DROP											: 'drop',
	FULL_WIDTH								: 'fullwidth',
	HEADER_BLUE								: 'hBlue'
}

const Selector = {
	BODY											: 'body',
	VIDEO_MAIN								: '#video-main',
	CONTACT_FORM							: '#contactForm',
	BUTTON_TOGGLE							: '.js-buttonToggle',
	HEADER										: '.js-header',
	BUTTON_OPEN_VIDEO 				: '.js-buttonOpenVideo',
	BUTTON_CLOSE_VIDEO				: '.js-buttonCloseVideo',
	LOOP_WRAPPER 							: '.js-loopWrapper',
	LOOP_WRAPPER_VIDEO 				: '.js-loopWrapperVideo',
	TEXT_BANNER_SLIDER 				: '.js-textBannerSlider',
	IMAGE_BANNER_SLIDER 			: '.js-imageBannerSlider'
}

class Site {
	constructor() {
		// Atributes
		this.myVideo = $('#video-main').length ? videojs('video-main') : null
		//Responsive
		this.mqTablet = matchMedia('(min-width: 768px)')
		this.mqTabletWide = matchMedia('(min-width: 992px)')

		// Methods
		this._calculateRatio(Selector.LOOP_WRAPPER)
		this._calculateRatio(Selector.LOOP_WRAPPER_VIDEO)
		this._initSlider()
		this._handleVideo()
		this._validForm()
		this._addEventListener()

	}

	_addEventListener() {
		//Action button toggle
		$(Selector.BUTTON_TOGGLE).on('click', (ev) => {
			console.log('open')
			$(Selector.BODY).toggleClass('is-active')
		})

		let hHeader = $(Selector.HEADER).outerHeight()

		//Change header on loaded page
		$(window).on('scroll', (ev) => {
			this._scrollHeader(hHeader)
		})

		//Change header on scroll
		$(window).on('scroll', (ev) => {
			this._scrollHeader(hHeader)
		})

		$(Selector.BUTTON_OPEN_VIDEO).on('click', (ev) => {
			this._openVideo()
		})

		$(Selector.BUTTON_CLOSE_VIDEO).on('click', (ev) => {
			this._closeVideo()
		})
	}

	_scrollHeader(hHeader) {
		let scrollTop = $(window).scrollTop()

		if (scrollTop >= hHeader) {
			$(Selector.HEADER).addClass(ClassName.FULL_WIDTH)
			$(Selector.HEADER).removeClass(ClassName.ANIMATE_DELAY)
		} else {
			$(Selector.HEADER).removeClass(ClassName.FULL_WIDTH)
		}

		if ( scrollTop >= (hHeader + 50) ) {
			$(Selector.HEADER).addClass(ClassName.DROP)
		} else {
			$(Selector.HEADER).removeClass(ClassName.DROP)
		}
	}

	_initSlider() {
		$(Selector.TEXT_BANNER_SLIDER).slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: true,
			fade: true,
			autoplay: true,
  		autoplaySpeed: 4500,
			asNavFor: Selector.IMAGE_BANNER_SLIDER
		})

		$(Selector.IMAGE_BANNER_SLIDER).slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			asNavFor: Selector.TEXT_BANNER_SLIDER,
			arrows: false,
			centerMode: true,
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 768,
					settings: {
						centerMode: false
					}
				}
			]
		})
	}

	_calculateRatio(elem) {
		let h = $(elem).outerHeight()
		let w = h * (16/9)

		$(elem).css({
			height: h,
			width: w
		})
	}

	_handleVideo() {
		if( this.mqTablet.matches ) {
			if ($(Selector.VIDEO_MAIN).length) {
				this.myVideo.muted(true)
				this.myVideo.autoplay(true)
				this.myVideo.controls(false)
				this.myVideo.loop(true)
			}
		} else {
			if ($(Selector.VIDEO_MAIN).length) {
				this.myVideo.muted(false)
				this.myVideo.autoplay(false)
				this.myVideo.controls(true)
				this.myVideo.loop(false)
			}
		}
	}

	_openVideo() {
		if( this.mqTablet.matches ) {
			//Hide header
			$(Selector.HEADER).addClass(ClassName.ANIMATE_HEADER_VIDEO).removeClass(ClassName.ANIMATE_DELAY)

			//Show button close
			$(Selector.BUTTON_CLOSE_VIDEO).show('slow')

			let h = $(Selector.LOOP_WRAPPER).outerHeight()

			$(Selector.LOOP_WRAPPER).css({
				width: '100%',
				height: h
			})

			if( this.mqTabletWide.matches ) {
				$(Selector.LOOP_WRAPPER).css({
					width: '100vw',
					height: h
				})
			}

		}

		this.myVideo.muted(false)
		this.myVideo.controls(true)
		//Video init 00:00
		this.myVideo.currentTime(0)
		this.myVideo.play()
		$(Selector.LOOP_WRAPPER).closest('.mask__component').addClass('is-playing')
	}

	_closeVideo() {
		this._calculateRatio(Selector.LOOP_WRAPPER)
		$(Selector.LOOP_WRAPPER).closest('.mask__component').removeClass('is-playing')
		$(Selector.HEADER).removeClass(ClassName.ANIMATE_HEADER_VIDEO).addClass(ClassName.ANIMATE_DELAY)
		this.myVideo.pause()
		this.myVideo.controls(false)
	}

	_validForm() {
		// Validate key entry
		$('#f-name').filter_input({ regex: '[a-z A-Z ñÑ áéíóú]' });
		$('#f-phone').filter_input({ regex: '[0-9]' });

		$(Selector.CONTACT_FORM).validate({
			rules: {
				'f-name': {
					required: true,
					minlength: 2
				},
				'f-email': {
					required: true,
					email: true
				},
				'f-phone': {
					required: true,
					digits: true,
					minlength: 7,
					maxlength: 9
				},
				'f-select': 'required',
				'f-message': {
					minlength: 2
				}
			},
			errorPlacement: (err, el) => {
				return false
			},
			submitHandler: (form) => {
				this._sendForm(form)
			}
		})
	}

	_sendForm(form) {
		let $form = $(form)
		let $button = $form.find('button')

		$button
			.text('ENVIANDO')
			.addClass('is-disabled')

		$.ajax({
			url: arbol_ajax_vars.ajaxurl,
			method: 'POST',
			data: {
				name: $('input[name="f-name"]').val(),
				email: $('input[name="f-email"]').val(),
				phone: $('input[name="f-phone"]').val(),
				service: $('select[name="f-select"]').val(),
				message: $('textarea[name="f-message"]').val(),
				action: 'form-arbol'
			}
		})
		.done((response) => {
			if (response === true) {
				$.magnificPopup.open({
					items: {
						src: '#modal-thanks',
						type: 'inline',
						closeBtnInside: true
					}
				})

				$button
					.text('ENVIAR')
					.removeClass('is-disabled')

				$form[0].reset()
			}
		})
	}
}

export default Site;

# Frontend Boilerplate

Template Frontend para proyectos web usando **Sass**, **Pug**, **ES6** y **Gulp**. Antes de continuar con el paso de **Instalación**, instalar [NodeJS](https://nodejs.org/es/) para el manejo de las dependencias.

## Instalación
Para el uso del template, instalar por primera y única vez en tu computador las siguientes dependencias de manera global: gulp, gulp-cli, node-sass y yarn; esto se logran con el siguiente comando:

```
npm install gulp gulp-cli node-sass yarn -g
```

En caso de usar S.O. Linux o MacOSX anteponer **sudo** al comando de arriba.

Clonar el template en tu carpeta principal de desarrollo. Dentro de tu template ya clonado y por consola, instalar las dependencias (desarrollo/producción) del proyecto con el siguiente comando:

```
yarn install // Instala las dependencias (ejem: jQuery)
```

Para levantar/visualizar el proyecto en tu navegador, y ver los cambios realtime usa el siguiente comando.

```
gulp // Colocar en el navegador http://localhost:8080
```

## Comandos adicionales

```
gulp images // comprime imágenes tomadas de la carpeta src/images y las envía a la carpeta dist/images
gulp fonts // copia las fuentes de la carpeta src/fonts y las envía a la carpeta dist/fonts
```